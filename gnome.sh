#!/bin/bash

sudo timedatectl set-ntp true
sudo hwclock --systohc

sudo reflector --sort rate -l 20 --save /etc/pacman.d/mirrorlist
sudo pacman -Sy

# sudo firewall-cmd --add-port=1025-65535/tcp --permanent
# sudo firewall-cmd --add-port=1025-65535/udp --permanent
# sudo firewall-cmd --reload
# sudo virsh net-autostart default


sudo pacman -S gdm gnome gnome-extra firefox gnome-tweaks sobs-studio vlc dina-font tamsyn-font bdf-unifont ttf-bitstream-vera ttf-dejavu ttf-droid gnu-free-fonts ttf-liberation ttf-linux-libertine noto-fonts ttf-roboto ttf-ubuntu-font-family ttf-monofur adobe-source-code-pro-fonts cantarell-fonts ttf-opensans ttf-junicode adobe-source-han-sans-otc-fonts adobe-source-han-serif-otc-fonts noto-fonts-cjk noto-fonts-emoji

# sudo flatpak install -y spotify
# sudo flatpak install -y kdenlive

sudo systemctl enable gdm
/bin/echo -e "\e[1;32mREBOOTING IN 5..4..3..2..1..\e[0m"
sleep 5
sudo reboot
